## Interface: 11506
## Title: Map Trajectory
## Notes: Displays player trajectory on maps
## SavedVariables: mapTrajConfig

Init.lua
Util.lua
Options.lua
Core.lua
