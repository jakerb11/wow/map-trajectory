--  Namespace for add on global functions
if mapTraj == nil then
  mapTraj = {}
  
  -- Initialize world map drawing frame
  -- This frame mirrors the "true" in-game world map, but allows this addon to draw on top of it
  mapTraj.mapFrame = CreateFrame("Frame", nil, WorldMapFrame:GetCanvas())
  mapTraj.mapFrame:SetAllPoints()
  mapTraj.mapFrame:SetFrameLevel(3000)
  
  -- Initialize laser
  mapTraj.laser = mapTraj.mapFrame:CreateLine()
  mapTraj.laser:SetDrawLayer("OVERLAY", -4)
  
  -- Initialize dots pool
  mapTraj.dots = {}
  mapTraj.dots.pool = {}
  mapTraj.dots.length = 0
end

-- Table of configuration saved data
if mapTrajConfig == nil then
  mapTrajConfig = {}
end
