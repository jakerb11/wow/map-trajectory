-- Set interface options default values
function mapTraj.setDefaultOptions()
  if mapTrajConfig.showOnWorldMap == nil then
    mapTrajConfig.showOnWorldMap = true
  end
  if mapTrajConfig.showOnMiniMap == nil then
    mapTrajConfig.showOnMiniMap = false
  end
  
  if mapTrajConfig.style == nil then
    mapTrajConfig.style = "dots"
  end
  
  if mapTrajConfig.color == nil then
    mapTrajConfig.color = {r = 1.0, g = 1.0, b = 1.0, a = 1.0}
  end
  
  -- Laser style specific configuration
  if mapTrajConfig.laser == nil then
    mapTrajConfig.laser = {}
  end
  if mapTrajConfig.laser.width == nil then
    mapTrajConfig.laser.width = 1
  end
  
  -- Dots style specific configuration
  if mapTrajConfig.dots == nil then
    mapTrajConfig.dots = {}
  end
  if mapTrajConfig.dots.size == nil then
    mapTrajConfig.dots.size = 3
  end
  if mapTrajConfig.dots.space == nil then
    mapTrajConfig.dots.space = 18
  end
  if mapTrajConfig.dots.speed == nil then
    mapTrajConfig.dots.speed = 20
  end
end

-- Create interface options panel
--   Interface panel is not setup until saved variables are loaded
function mapTraj.interfaceOptions()
  local configPanel = CreateFrame("Frame", nil, InterfaceOptionsFramePanelContainer)
  configPanel.name = "Map Trajectory"
  
  local category = Settings.RegisterCanvasLayoutCategory(configPanel, configPanel.name)
  Settings.RegisterAddOnCategory(category)
  
  -- Setup slash command
  SLASH_MAPTRAJ1 = "/maptraj"
  SlashCmdList["MAPTRAJ"] = function(arg)
    Settings.OpenToCategory(category:GetID())
  end

  -- Configuration panel title
  local title = configPanel:CreateFontString(nil, "ARTWORK", "GameFontNormalLarge")
  title:SetPoint("TOPLEFT", 16, -16)
  title:SetText("Map Trajectory")
  
  -- Display player trajectory indicator on world map checkbox
  local showOnWorldMapCheckButton = CreateFrame("CheckButton", nil, configPanel, "InterfaceOptionsCheckButtonTemplate")
  showOnWorldMapCheckButton:SetPoint("TOPLEFT", title, "BOTTOMLEFT", 0, -16)
  showOnWorldMapCheckButton.Text:SetText("Show on World Map")
  showOnWorldMapCheckButton.tooltipText = "Select to display trajectory indicator on world map"
  showOnWorldMapCheckButton:SetChecked(mapTrajConfig.showOnWorldMap)
  showOnWorldMapCheckButton:SetScript("OnClick", function(self, button, ...)
    mapTrajConfig.showOnWorldMap = showOnWorldMapCheckButton:GetChecked()
  end)
  
  -- Display player trajectory indicator on mini-map checkbox
  local showOnMiniMapCheckButton = CreateFrame("CheckButton", nil, configPanel, "InterfaceOptionsCheckButtonTemplate")
  showOnMiniMapCheckButton:SetPoint("TOPLEFT", showOnWorldMapCheckButton, "TOPRIGHT", 128, 0)
  showOnMiniMapCheckButton.Text:SetText("Show on Mini-Map (COMING SOON)")
  showOnMiniMapCheckButton.tooltipText = "Select to display trajectory indicator on mini-map"
  showOnMiniMapCheckButton:SetChecked(mapTrajConfig.showOnMiniMap)
  -- Disable this option as it is not yet supported
  showOnMiniMapCheckButton:SetEnabled(false)
  showOnMiniMapCheckButton:SetScript("OnClick", function(self, button, ...)
    mapTrajConfig.showOnMiniMap = showOnMiniMapCheckButton:GetChecked()
  end)
  
  --
  -- Line Customization
  --
  
  -- Line customization label
  local lineOptionsHeading = configPanel:CreateFontString(nil, "ARTWORK", "GameFontNormal")
  lineOptionsHeading:SetPoint("TOPLEFT", showOnWorldMapCheckButton, "BOTTOMLEFT", 0, -24)
  lineOptionsHeading:SetText("Line Options")
  
  -- Trajectory style drop down
  local styleLabel = configPanel:CreateFontString(nil, "ARTWORK", "GameFontNormalSmall")
  styleLabel:SetPoint("TOPLEFT", lineOptionsHeading, "BOTTOMLEFT", 0, -16)
  styleLabel:SetText("Style")
  local styleDropDown = CreateFrame("Frame", nil, configPanel, "UIDropDownMenuTemplate")
  styleDropDown:SetPoint("TOPLEFT", styleLabel, "BOTTOMLEFT", 0, -8)
  UIDropDownMenu_SetWidth(styleDropDown, 112)
  UIDropDownMenu_SetText(styleDropDown, mapTrajConfig.style)
  UIDropDownMenu_Initialize(styleDropDown, function(frame, level, menuList)
    local info = UIDropDownMenu_CreateInfo()
    info.checked = false
    
    info.func = function(self, arg1, arg2, checked)
      UIDropDownMenu_SetText(frame, arg1)
      mapTrajConfig.style = arg1
      
      -- Refresh style options
      refreshStyleOptions()
    end
    
    -- Add laser option
    info.text = "laser"
    info.arg1 = info.text
    UIDropDownMenu_AddButton(info)
    
    -- Add dots option
    info.text = "dots"
    info.arg1 = info.text
    UIDropDownMenu_AddButton(info)
  end)
  
  -- Trajectory color selector
  local colorLabel = configPanel:CreateFontString(nil, "ARTWORK", "GameFontNormalSmall")
  colorLabel:SetPoint("TOPLEFT", styleDropDown, "BOTTOMLEFT", 0, -16)
  colorLabel:SetText("Color")
  local colorSelector = configPanel:CreateTexture(nil, "ARTWORK")
  colorSelector:SetPoint("TOPLEFT", colorLabel, "BOTTOMLEFT", 0, -16)
  colorSelector:SetSize(48, 24)
  colorSelector:SetColorTexture(mapTrajConfig.color.r, mapTrajConfig.color.g, mapTrajConfig.color.b, mapTrajConfig.color.a)
  colorSelector:SetMouseClickEnabled(true)
  colorSelector:SetScript("OnMouseUp", function()
    ColorPickerFrame:Hide()
    
    ColorPickerFrame.previousValues = {mapTrajConfig.color.r, mapTrajConfig.color.g, mapTrajConfig.color.b, mapTrajConfig.color.a}
    ColorPickerFrame:SetColorRGB(mapTrajConfig.color.r, mapTrajConfig.color.g, mapTrajConfig.color.b)
    
    ColorPickerFrame.hasOpacity = true
    ColorPickerFrame.opacity = mapTrajConfig.color.a
    
    ColorPickerFrame.swatchFunc = function()
      mapTrajConfig.color.r, mapTrajConfig.color.g, mapTrajConfig.color.b = ColorPickerFrame:GetColorRGB()
      colorSelector:SetColorTexture(mapTrajConfig.color.r, mapTrajConfig.color.g, mapTrajConfig.color.b)
    end
    ColorPickerFrame.opacityFunc = function()
      mapTrajConfig.color.a = OpacitySliderFrame:GetValue()
    end
    ColorPickerFrame.cancelFunc = function(previousValues)
      mapTrajConfig.color.r, mapTrajConfig.color.g, mapTrajConfig.color.b, mapTrajConfig.color.a = unpack(ColorPickerFrame.previousValues)
      colorSelector:SetColorTexture(mapTrajConfig.color.r, mapTrajConfig.color.g, mapTrajConfig.color.b)
    end
    
    ColorPickerFrame:Show()
  end)
  
  -- Laser line style width
  local laserWidthLabel = configPanel:CreateFontString(nil, "ARTWORK", "GameFontNormalSmall")
  laserWidthLabel:SetPoint("TOPLEFT", colorSelector, "BOTTOMLEFT", 0, -16)
  laserWidthLabel:SetText("Width of the laser line")
  local laserWidthSlider = CreateFrame("Slider", nil, configPanel, "OptionsSliderTemplate")
  laserWidthSlider:SetPoint("TOPLEFT", laserWidthLabel, "BOTTOMLEFT", 0, -8)
  laserWidthSlider:SetWidth(112)
  laserWidthSlider:SetMinMaxValues(1, 5)
  laserWidthSlider.Low:SetText(1)
  laserWidthSlider.High:SetText(5)
  laserWidthSlider:SetValueStep(1)
  laserWidthSlider:SetObeyStepOnDrag(true)
  laserWidthSlider:SetValue(mapTrajConfig.laser.width)
  local laserWidthValue = configPanel:CreateFontString(nil, "ARTWORK", "GameFontNormalSmall")
  laserWidthValue:SetPoint("TOPLEFT", laserWidthSlider, "BOTTOMLEFT", 0, -8)
  laserWidthValue:SetWidth(112)
  laserWidthValue:SetText(laserWidthSlider:GetValue())
  laserWidthValue:SetJustifyH("CENTER")
  laserWidthSlider:SetScript("OnValueChanged", function(self, value, ...)
    laserWidthValue:SetText(value)
    mapTrajConfig.laser.width = laserWidthSlider:GetValue()
  end)
  
  -- Dots size
  local dotsSizeLabel = configPanel:CreateFontString(nil, "ARTWORK", "GameFontNormalSmall")
  dotsSizeLabel:SetPoint("TOPLEFT", colorSelector, "BOTTOMLEFT", 0, -16)
  dotsSizeLabel:SetText("Dot size")
  local dotsSizeSlider = CreateFrame("Slider", nil, configPanel, "OptionsSliderTemplate")
  dotsSizeSlider:SetPoint("TOPLEFT", dotsSizeLabel, "BOTTOMLEFT", 0, -8)
  dotsSizeSlider:SetWidth(112)
  dotsSizeSlider:SetMinMaxValues(1, 10)
  dotsSizeSlider.Low:SetText(1)
  dotsSizeSlider.High:SetText(10)
  dotsSizeSlider:SetValueStep(1)
  dotsSizeSlider:SetObeyStepOnDrag(true)
  dotsSizeSlider:SetValue(mapTrajConfig.dots.size)
  local dotsSizeValue = configPanel:CreateFontString(nil, "ARTWORK", "GameFontNormalSmall")
  dotsSizeValue:SetPoint("TOPLEFT", dotsSizeSlider, "BOTTOMLEFT", 0, -8)
  dotsSizeValue:SetWidth(112)
  dotsSizeValue:SetText(dotsSizeSlider:GetValue())
  dotsSizeValue:SetJustifyH("CENTER")
  dotsSizeSlider:SetScript("OnValueChanged", function(self, value, ...)
    dotsSizeValue:SetText(value)
    mapTrajConfig.dots.size = dotsSizeSlider:GetValue()
  end)
  
  -- Dots spacing
  local dotsSpaceLabel = configPanel:CreateFontString(nil, "ARTWORK", "GameFontNormalSmall")
  dotsSpaceLabel:SetPoint("TOPLEFT", dotsSizeLabel, "TOPRIGHT", 128, 0)
  dotsSpaceLabel:SetText("Dot spacing")
  local dotsSpaceSlider = CreateFrame("Slider", nil, configPanel, "OptionsSliderTemplate")
  dotsSpaceSlider:SetPoint("TOPLEFT", dotsSpaceLabel, "BOTTOMLEFT", 0, -8)
  dotsSpaceSlider:SetWidth(112)
  dotsSpaceSlider:SetMinMaxValues(1, 50)
  dotsSpaceSlider.Low:SetText(1)
  dotsSpaceSlider.High:SetText(50)
  dotsSpaceSlider:SetValueStep(1)
  dotsSpaceSlider:SetObeyStepOnDrag(true)
  dotsSpaceSlider:SetValue(mapTrajConfig.dots.space)
  local dotsSpaceValue = configPanel:CreateFontString(nil, "ARTWORK", "GameFontNormalSmall")
  dotsSpaceValue:SetPoint("TOPLEFT", dotsSpaceSlider, "BOTTOMLEFT", 0, -8)
  dotsSpaceValue:SetWidth(112)
  dotsSpaceValue:SetText(dotsSpaceSlider:GetValue())
  dotsSpaceValue:SetJustifyH("CENTER")
  dotsSpaceSlider:SetScript("OnValueChanged", function(self, value, ...)
    dotsSpaceValue:SetText(value)
    mapTrajConfig.dots.space = dotsSpaceSlider:GetValue()
  end)
  
  -- Dots speed
  local dotsSpeedLabel = configPanel:CreateFontString(nil, "ARTWORK", "GameFontNormalSmall")
  dotsSpeedLabel:SetPoint("TOPLEFT", dotsSizeValue, "BOTTOMLEFT", 0, -16)
  dotsSpeedLabel:SetText("Dot speed")
  local dotsSpeedSlider = CreateFrame("Slider", nil, configPanel, "OptionsSliderTemplate")
  dotsSpeedSlider:SetPoint("TOPLEFT", dotsSpeedLabel, "BOTTOMLEFT", 0, -8)
  dotsSpeedSlider:SetWidth(112)
  dotsSpeedSlider:SetMinMaxValues(0, 100)
  dotsSpeedSlider.Low:SetText(0)
  dotsSpeedSlider.High:SetText(100)
  dotsSpeedSlider:SetValueStep(1)
  dotsSpeedSlider:SetObeyStepOnDrag(true)
  dotsSpeedSlider:SetValue(mapTrajConfig.dots.speed)
  local dotsSpeedValue = configPanel:CreateFontString(nil, "ARTWORK", "GameFontNormalSmall")
  dotsSpeedValue:SetPoint("TOPLEFT", dotsSpeedSlider, "BOTTOMLEFT", 0, -8)
  dotsSpeedValue:SetWidth(112)
  dotsSpeedValue:SetText(dotsSpeedSlider:GetValue())
  dotsSpeedValue:SetJustifyH("CENTER")
  dotsSpeedSlider:SetScript("OnValueChanged", function(self, value, ...)
    dotsSpeedValue:SetText(value)
    mapTrajConfig.dots.speed = dotsSpeedSlider:GetValue()
  end)
  
  function refreshStyleOptions()
    -- Hide all style dependent options
    laserWidthLabel:Hide()
    laserWidthSlider:Hide()
    laserWidthValue:Hide()
    
    dotsSizeLabel:Hide()
    dotsSizeSlider:Hide()
    dotsSizeValue:Hide()
    
    dotsSpaceLabel:Hide()
    dotsSpaceSlider:Hide()
    dotsSpaceValue:Hide()
    
    dotsSpeedLabel:Hide()
    dotsSpeedSlider:Hide()
    dotsSpeedValue:Hide()
    
    -- Enable options for selected style
    if mapTrajConfig.style == "laser" then
      laserWidthLabel:Show()
      laserWidthSlider:Show()
      laserWidthValue:Show()
    elseif mapTrajConfig.style == "dots" then
      dotsSizeLabel:Show()
      dotsSizeSlider:Show()
      dotsSizeValue:Show()
      
      dotsSpaceLabel:Show()
      dotsSpaceSlider:Show()
      dotsSpaceValue:Show()
      
      dotsSpeedLabel:Show()
      dotsSpeedSlider:Show()
      dotsSpeedValue:Show()
    end
  end
  
  -- Show correct style options initially
  refreshStyleOptions()

  configPanel:SetScript("OnHide", function()
    -- Update options
    mapTraj.updateOptions()
  end)
end
