--
-- Data
--

-- Main frame
local mainFrame = CreateFrame("Frame")

-- List of all events used in this addon
local events = {}

-- Trajectory end points
local startPoint = nil
local endPoint = nil
local length = nil

-- Map data
local currentMap = {}
-- id
-- continentID
-- north
-- south
-- west
-- east
-- northWest
-- northEast
-- southWest
-- southEast
-- width
-- height

--
-- Functions
--

local function updateMapInfo()
  local id = WorldMapFrame:GetMapID()
  
  if currentMap.id == nil or id ~= currentMap.id then
    currentMap.id = id
    
    -- Get world positions for map corners for scaling
    -- In the WoW world coordinate system, the x-axis points north and the y-axis points west
    -- Therefore the point returned from C_Map.GetWorldPosFromMapPos has:
    -- x - vertical coordinate
    -- y - horizontal coordinate
    local continentID, point = C_Map.GetWorldPosFromMapPos(currentMap.id, {x = 0, y = 0})
    currentMap.north, currentMap.west = point:GetXY()
    continentID, point = C_Map.GetWorldPosFromMapPos(currentMap.id, {x = 1, y = 1})
    currentMap.south, currentMap.east = point:GetXY()
    
    -- Continent ID is necessary to translate world positions back to map coordinates
    currentMap.continentID = continentID
    
    -- Create points at each corner
    currentMap.northWest = CreateVector2D(currentMap.north, currentMap.west)
    currentMap.northEast = CreateVector2D(currentMap.north, currentMap.east)
    currentMap.southWest = CreateVector2D(currentMap.south, currentMap.west)
    currentMap.southEast = CreateVector2D(currentMap.south, currentMap.east)
    
    -- World map size
    currentMap.width = mapTraj.mapFrame:GetWidth()
    currentMap.height = mapTraj.mapFrame:GetHeight()
  end
end

-- Returns the point where a line intersect and line segment; nil otherwise
-- Player position and direction definte a ray with infinite length originating from the player
-- The points p & q form a line segment along the world map edge
function mapTraj.segmentIntersect(playerPos, playerDir, p, q)
  -- There are many resources online for computing the intersection of lines and line segments
  -- This particular alogirthm was adapted from the stackoverflow discussion held here:
  -- https://stackoverflow.com/questions/563198/how-do-you-detect-where-two-line-segments-intersect
  
  -- Vector connecting end points
  local edgeDir = CreateVector2D(q.x - p.x, q.y - p.y)
  
  -- Vectors are collinear or parallel (i.e. don't intersect) if cross product between them is zero
  local dirCross = playerDir.x * edgeDir.y - playerDir.y * edgeDir.x
  if dirCross ~= 0 then
    -- Vector connecting starting points used in subsequent calculations
    local delta = CreateVector2D(p.x - playerPos.x, p.y - playerPos.y)
    -- Compute intersection location in terms of each line (barycentric coordinate)
    local rayIntersection = (delta.x * edgeDir.y - delta.y * edgeDir.x) / dirCross
    local segmentIntersection = (delta.x * playerDir.y - delta.y * playerDir.x) / dirCross
    
    -- The intersection must occur in the positive direction of the ray, and between the endpoints of the segment
    if rayIntersection >= 0 and segmentIntersection >= 0 and segmentIntersection <= 1 then
      return rayIntersection
    end
  end
end

local function updateEndPoints(playerPos, playerDir, dist)
  local point = CreateVector2D(playerPos.x + dist * playerDir.x, playerPos.y + dist * playerDir.y)
  local _, mapPoint = C_Map.GetMapPosFromWorldPos(currentMap.continentID, point, currentMap.id)
  
  if endPoint == nil then
    -- This is the first intersection
    endPoint = point
    length = dist
  else
    -- This is the second intersection
    if dist < length then
      -- The new intersection is closer to the player
      startPoint = point
    else
      -- The new intersection is further from the player
      startPoint = endPoint
      endPoint = point
    end
  end
end

local function disableTrajectory()
  mapTraj.laser:Hide()
  
  for i = 1, mapTraj.dots.length do
    mapTraj.dots.pool[i]:Hide()
  end
end

local function enableTrajectory()
  -- Show selected trajectory style
  if mapTrajConfig.style == "laser" then
    disableTrajectory()
    mapTraj.laser:Show()
  end
end

local function drawTrajectory(startPoint, endPoint)
  if mapTrajConfig.style == "laser" then
    mapTraj.laser:SetStartPoint("TOPLEFT", mapTraj.mapFrame, startPoint.x, startPoint.y)
    mapTraj.laser:SetEndPoint("TOPLEFT", mapTraj.mapFrame, endPoint.x, endPoint.y)
  elseif mapTrajConfig.style == "dots" then
    -- Vector from start to finish
    local delta = CreateVector2D(endPoint.x - startPoint.x, endPoint.y - startPoint.y)
    -- Trajectory distance
    local length = delta:GetLength()
    -- Trajectory direction
    local dir = CreateVector2D(delta.x / length, delta.y / length)
    
    -- Initial step to first dot
    local step = 10
    
    -- Adjust for current phase if dots are animated
    if mapTrajConfig.dots.speed > 0 then
      -- Wavelength (time for dots animation to repeat)
      local lambda = mapTrajConfig.dots.space / mapTrajConfig.dots.speed
      -- Fraction of the wavelength completed
      local phase = (GetTime() % lambda) / lambda
      
      step = step + phase * mapTrajConfig.dots.space
    end
    
    local i = 1
    while step < length do
      -- Create a dot along the trajectory
      if mapTraj.dots.length < i then
        -- Add new dot to pool
        mapTraj.dots.pool[i] = CreateFrame("Frame", nil, mapTraj.mapFrame)
        mapTraj.dots.pool[i]:SetPoint("CENTER")
        mapTraj.dots.pool[i].texture = mapTraj.dots.pool[i]:CreateTexture()
        mapTraj.dots.pool[i].texture:SetAllPoints()
        mapTraj.dots.pool[i].texture:SetDrawLayer("OVERLAY", -4)
        mapTraj.dots.pool[i].texture:SetTexture("Interface\\AddOns\\MapTrajectory\\resource\\image\\circle.tga")
        
        -- User configuration
        mapTraj.dots.pool[i].texture:SetVertexColor(mapTrajConfig.color.r, mapTrajConfig.color.g, mapTrajConfig.color.b, mapTrajConfig.color.a)
        mapTraj.dots.pool[i]:SetSize(mapTrajConfig.dots.size, mapTrajConfig.dots.size)
        
        mapTraj.dots.length = mapTraj.dots.length + 1
      end
      
      -- Set dot to position
      mapTraj.dots.pool[i]:SetPoint("CENTER", mapTraj.mapFrame, "TOPLEFT", startPoint.x + step * dir.x, startPoint.y + step * dir.y)
      mapTraj.dots.pool[i]:Show()
      
      -- Step towards the end
      step = step + mapTrajConfig.dots.space
      i = i + 1
    end
    
    -- Hide any unused dots
    while i <= mapTraj.dots.length do
      mapTraj.dots.pool[i]:Hide()
      i = i + 1
    end
  end
end

local function updateTrajectory()
  updateMapInfo()
  
  if currentMap.id == 947 then
    -- Disable trajectory on full Azeroth world map
    -- This map doesn't play nice due to the WoW API providing the same instance/continent ID for Kalimdor and Azeroth
    disableTrajectory()
    return
  end
  
  -- Determine trajectory start position (player position on current map)
  local playerX, playerY, _, playerInstance = UnitPosition("player")
  
  if playerX == nil or playerY == nil then
    -- Player position is unavailable (in restricted zone -- instance/battleground/arena)
    disableTrajectory()
    return
  end
  
  mapInstance = C_Map.GetWorldPosFromMapPos(currentMap.id, {x = 0, y = 0})
  if playerInstance ~= mapInstance then
    -- Player instance and map instance are incompatible
    disableTrajectory()
    return
  end
  
  local playerPos = CreateVector2D(playerX, playerY)
  -- Player facing is in radians, [0, 2π] range, 0 is north, and values increase counter-clockwise
  local playerFacing = GetPlayerFacing()
  local playerDir = CreateVector2D(math.cos(playerFacing), math.sin(playerFacing))
  
  --
  -- Find trajectory end points
  --
  
  -- In this section we find the location(s) at which a line starting from the player's position and 
  -- This code handles all (3) cases:
  -- 3) The player is outside the map edges and there are no intersections
  -- 1) The player is inside the map edges and there is one intersection
  -- 2) The player is outside the map edges and there are no intersections
  
  -- Start point defaults to player position
  startPoint = playerPos
  endPoint = nil
  trajLength = nil
  
  -- Check intersection with each map edge
  local startCorner = currentMap.northEast
  for i, endCorner in ipairs({currentMap.northWest, currentMap.southWest, currentMap.southEast, currentMap.northEast}) do
    local dist = mapTraj.segmentIntersect(playerPos, playerDir, startCorner, endCorner)
    if dist ~= nil then
      updateEndPoints(playerPos, playerDir, dist)
    end
    startCorner = endCorner
  end
  
  if endPoint == nil then
    -- Trajectory does not appear on map
    disableTrajectory()
    return
  end
  
  -- Translate world positions back to map coordinates
  _, startPoint = C_Map.GetMapPosFromWorldPos(currentMap.continentID, startPoint, currentMap.id)
  _, endPoint = C_Map.GetMapPosFromWorldPos(currentMap.continentID, endPoint, currentMap.id)
    
  -- Scale world position to frame coordinates
  startPoint.x = startPoint.x * currentMap.width
  startPoint.y = -startPoint.y * currentMap.height
  endPoint.x = endPoint.x * currentMap.width
  endPoint.y = -endPoint.y * currentMap.height
    
  drawTrajectory(startPoint, endPoint)
  enableTrajectory()
end

-- Updates parameters based on user interface options
function mapTraj.updateOptions()
  --
  -- World map
  --
  
  disableTrajectory()
  if mapTrajConfig.showOnWorldMap then
    mapTraj.mapFrame:SetScript("OnUpdate", updateTrajectory)
    enableTrajectory()
  else
    mapTraj.mapFrame:SetScript("OnUpdate", nil)
  end
  
  --
  -- Trajectory style
  --
  
  -- Update laser based on user config
  mapTraj.laser:SetColorTexture(mapTrajConfig.color.r, mapTrajConfig.color.g, mapTrajConfig.color.b, mapTrajConfig.color.a)
  mapTraj.laser:SetThickness(mapTrajConfig.laser.width)
  
  -- Update dots based on user config
  for i = 1, mapTraj.dots.length do
    mapTraj.dots.pool[i].texture:SetVertexColor(mapTrajConfig.color.r, mapTrajConfig.color.g, mapTrajConfig.color.b, mapTrajConfig.color.a)
    mapTraj.dots.pool[i]:SetSize(mapTrajConfig.dots.size, mapTrajConfig.dots.size)
  end
end

--
-- Event handlers
--

-- Add On loaded initialization event
events["ADDON_LOADED"] = function(addOnName)
  if addOnName == "MapTrajectory" then
    print("|cFF64AFFFMap Trajectory|r loaded (/maptraj)")
  end
end

-- Saved variables loaded event
events["VARIABLES_LOADED"] = function()
  -- Set default options after saved variables have been loaded
  mapTraj.setDefaultOptions()
  -- Create interface options panel after saved variables loaded
  mapTraj.interfaceOptions()
  -- Update addon parameters based on user interface options
  mapTraj.updateOptions()
end

-- Register events
mainFrame:RegisterEvent("ADDON_LOADED")
mainFrame:RegisterEvent("VARIABLES_LOADED")

-- Main event handler
local function eventHandler(self, event, ...)
  events[event](...)
end

-- Set frame script event handler
mainFrame:SetScript("OnEvent", eventHandler)