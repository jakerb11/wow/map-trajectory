# Map Trajectory

The Map Trajectory add-on adds a configurable indicator to the in-game maps
(world and mini).  It is similar to the trajectory indicator added to Retail
WoW for the Dragonflight expansion.

## Download/Installation Instructions

1. Make sure you have the main branch selected
2. Click the download button
3. Download as a .zip file (maptrajectory-main.zip)
4. Extract the files somewhere
5. Navigate to the directory called MapTrajectory
6. Copy or move the MapTrajectory directory to you WoW addons folder

## Interface Options

The Map Trajectory options window can be found in the in-game user interface
panel.  The slash command, /maptraj, can also be used to access the user
options panel.

### Display on World Map

The display on world map check box toggles the displaying of the trajectory
indicator on the world map.  When enabled, the trajectory will be shown on the
world map.  When disabled, the trajectory will not be shown on the world map.

The world map display can also be toggled via a button on the world map screen.

### Display on Mini-Map

The display on mini-map check box toggles the displaying of the trajectory
indicator on the mini-map.  When enabled, the trajectory will be shown on the
mini-map.  When disabled, the trajectory will not be shown on the mini-map.

The mini-map display can also be toggled via a mini-map button.

### Trajectory Indicator

Select between display styles and colors for the trajectory indicator.